﻿using DataApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using DataApp.JSON;


namespace DataApp.Controllers
{
    public class DataController : ApiController
    {
        //http://weblogs.asp.net/scottgu/tip-trick-building-a-tojson-extension-method-using-net-3-5
        //TODO: Change the JSON output in the JSONHelper class
        Data d = new Data();

        public Data getMockTicketsSubmittedLastMonth()
        {
            int[] d = { 5, 2, 6, 8, 4, 3, 5, 2, 1, 5, 6, 7, 3, 2, 4, 5, 6, 3, 2, 5, 6, 7, 8, 6, 5, 4, 3, 2, 1, 3 };
            return new Data { TicketsSubmittedLastMonth = d };
        }

        [HttpGet]
        public String TicketsLastMonth()
        {
            retrieveData();
            return (d.ToJSON());
        }

        public void retrieveData()
        {
            SqlConnection con;
            SqlCommand cmd;
            SqlDataReader dr;
            con = new SqlConnection("user id=CMSAdmin;password=Carl1sle;server=wwwdb;Trusted_Connection=yes;database=nService4;connection timeout=30");
            con.Open();


            cmd = new SqlCommand("SELECT TOP 30 TicketsSubmittedPreviousMonth FROM SQDC_Board ORDER BY BoardDate DESC",con);
            dr = cmd.ExecuteReader();
            Queue<int> results = new Queue<int>();
            while (dr.Read())
            {
                results.Enqueue((int)dr["TicketsSubmittedPreviousMonth"]);
            }
            int[] d = new int[results.Count];
            int count = results.Count;
            for (int i = 0; i < count; i++)
            {
                d[i] = results.Dequeue() ;
            }
            Data data = new Data();
            data.TicketsSubmittedLastMonth = d;
            //return data;
            this.d = data;
        }

    }
}
